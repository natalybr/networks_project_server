const http = require('http');
traceroute = require('traceroute');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
   var ip = req.url.substring(1, req.url.length)
   console.log(ip);
  res.setHeader('Content-Type', 'application/json');
  traceroute.trace(ip, function (err,hops) {
      if (!err) {
        var hops_str = JSON.stringify(hops);
        console.log(ip +" => "+hops_str);
        res.end(hops_str);
      }
      else {
        console.log(err);
        res.end("");
      }
    });
});



server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});